$(document).ready(function (){
    $(".advantage-block__list, .duffbeer__list").slick({
        mobileFirst: true,
        draggable: true,
        waitForAnimate: false,
        touchThreshold: 10,
        centerMode: false,
        slidesToShow: 1,
        dots: true,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 700,
                slidesToShow: 2,
            },
            {
                breakpoint: 1019,
                settings: "unslick"
            }
        ]
    });
})
